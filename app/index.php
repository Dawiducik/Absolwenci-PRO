<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="MDB/css/bootstrap.min.css">
    <link rel="stylesheet" href="MDB/css/mdb.min.css">
    <link rel="stylesheet" href="MDB/css/style.css">
    <title>Absolwenci.PRO</title>
</head>
<body class="mdb-color darken-1">    
    <!-- <div class="row">
        <div class="col-lg-6">
            <h1 class="py-3 my-0 white-text">BAZA ABSOLWENTÓW ZST MIELEC</h1>
        </div>
        <div class="col-lg-6">

        </div> -->
        <nav class="navbar navbar-dark mdb-color justify-content-between white-text">
        <span class="navbar-brand align-self-center">BAZA ABSOLWENTÓW ZST MIELEC</span>
        <div class="wrapper">
            <button class="btn btn-outline-white">Strona ZST</button>
            <button class="btn btn-outline-white">Kontakt</button>
        </div>
        </nav>
    </div>
    <div class="container">
        <h4 class="mt-3 text-center white-text">Wybierz Rocznik</h4>
        <div class="wrapper d-flex flex-wrap justify-content-center">
            <button type="button" class="btn btn-outline-white">0001</button>
            <button type="button" class="btn btn-outline-white">0002</button>
            <button type="button" class="btn btn-outline-white">0003</button>
            <button type="button" class="btn btn-outline-white">0004</button>
            <button type="button" class="btn btn-outline-white">0005</button>
            <button type="button" class="btn btn-outline-white">0006</button>
            <button type="button" class="btn btn-outline-white">0007</button>
            <button type="button" class="btn btn-outline-white">0008</button>
            <button type="button" class="btn btn-outline-white">0009</button>
            <button type="button" class="btn btn-outline-white">0010</button>
            <button type="button" class="btn btn-outline-white">0011</button>
            <button type="button" class="btn btn-outline-white">0012</button>
            <button type="button" class="btn btn-outline-white">0013</button>
            <button type="button" class="btn btn-outline-white">0014</button>
            <button type="button" class="btn btn-outline-white">0015</button>
            <button type="button" class="btn btn-outline-white">0016</button>
            <button type="button" class="btn btn-outline-white">0017</button>
            <button type="button" class="btn btn-outline-white">0018</button>
            <button type="button" class="btn btn-outline-white">0019</button>
            <button type="button" class="btn btn-outline-white">0020</button>
            <button type="button" class="btn btn-outline-white">0021</button>
            <button type="button" class="btn btn-outline-white">0022</button>
            <button type="button" class="btn btn-outline-white">0023</button>
            <button type="button" class="btn btn-outline-white">0024</button>
            <!-- button[type="button" class="btn btn-outline-white"]{$$$$} -->
        </div>
    </div>
    <script src="MDB/js/jquery-3.1.1.min.js"></script>
    <script src="MDB/js/bootstrap.min.js"></script>
    <script src="MDB/js/mdb.min.js"></script>
    <script src="MDB/js/popper.min.js"></script>
</body>
</html>