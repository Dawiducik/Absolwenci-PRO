CREATE TABLE `Klasy` (
	`ID` INT NOT NULL AUTO_INCREMENT,
	`SzkolaID` INT NOT NULL,
	`Skrot` VARCHAR(255) NOT NULL,
	`Pelna_Nazwa` TEXT NOT NULL,
	`Rok` INT NOT NULL,
	`Wychowawca` VARCHAR(255) NOT NULL,
	PRIMARY KEY (`ID`)
);

CREATE TABLE `Uczniowie` (
	`ID` INT NOT NULL AUTO_INCREMENT,
	`Imie` VARCHAR(255) NOT NULL,
	`Nazwisko` VARCHAR(255) NOT NULL,
	`KlasaID` INT NOT NULL,
	`Dodano_Przez` INT NOT NULL,
	`Utworzono_o` TIMESTAMP NOT NULL,
	`Zmodyfikowano_o` TIMESTAMP NOT NULL,
	PRIMARY KEY (`ID`)
);

CREATE TABLE `Szkoly` (
	`ID` INT NOT NULL AUTO_INCREMENT,
	`Nazwa` VARCHAR(255) NOT NULL UNIQUE,
	PRIMARY KEY (`ID`)
);

CREATE TABLE `Uzytkownicy` (
	`ID` INT NOT NULL AUTO_INCREMENT,
	`Login` VARCHAR(255) NOT NULL,
	`Haslo` TEXT NOT NULL,
	`Nazwisko` VARCHAR(255),
	`Klasa` VARCHAR(255),
	`Rola` VARCHAR(255) NOT NULL,
	PRIMARY KEY (`ID`)
);

ALTER TABLE `Klasy` ADD CONSTRAINT `Klasy_fk0` FOREIGN KEY (`SzkolaID`) REFERENCES `Szkoly`(`ID`);

ALTER TABLE `Uczniowie` ADD CONSTRAINT `Uczniowie_fk0` FOREIGN KEY (`KlasaID`) REFERENCES `Klasy`(`ID`);

ALTER TABLE `Uczniowie` ADD CONSTRAINT `Uczniowie_fk1` FOREIGN KEY (`Dodano_Przez`) REFERENCES `Uzytkownicy`(`ID`);


INSERT INTO `Szkoly` 	(`Nazwa`)
			VALUES		('Technikum Nr 3'),
						('Liceum Nr 3'),
						('Zasadnicza Szkoła Zawodowa');